import java.math.BigInteger;
import java.util.Scanner;

public class DSA {
    public static void main(String[] args) {
        Key key = new Key();
        key.generateKey();
        BigInteger r = key.generateR();
        System.out.println("Enter message: ");
        Scanner in = new Scanner(System.in);
        byte [] data = in.nextLine().getBytes();
        BigInteger sig = key.generateS(r,data);
        //data[0] = 'H';
        System.out.println("Ver: " + key.verify(data,r,sig));
        System.out.println("Ver expecting false: " + key.verify(data,r,sig.add(BigInteger.ONE)));
    }
}
